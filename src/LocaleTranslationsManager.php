<?php
/**
 * @file
 */

namespace Drupal\locale_trans_file;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\CachedStorage;
use Drupal\Core\Config\ExtensionInstallStorage;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

class LocaleTranslationsManager {
  const CACHE_BIN = 'locale_trans_file';
  protected $localeConfigManager;
  protected $languageManager;
  protected $localeInstallStorage;
  protected $configStorage;
  protected $cache;
  protected $moduleHandler;

  public function __construct($root,
                              ExtensionInstallStorage $locale_install_storage,
                              LanguageManagerInterface $language_manager,
                              CachedStorage $config_storage,
                              ModuleHandlerInterface $module_handler,
                              CacheBackendInterface $locale_translations_cache)
  {
    $this->languageManager = $language_manager;
    $this->localeInstallStorage = $locale_install_storage;
    $this->configStorage = $config_storage;
    $this->cache = $locale_translations_cache;
    $this->moduleHandler = $module_handler;
  }

  /**
   * @return mixed
   */
  public function rebuild() {
    // Clear all cached data
    $this->cache->deleteAll();

    $languages = $this->languageManager->getLanguages();
    $extensions = $this->configStorage->read('core.extension');
    $all_files = $this->localeInstallStorage->listAll();

    foreach ($languages as $lang) {
      $lang_id = $lang->getId();
      $length = strlen($lang_id);
      $language_files = array_filter($all_files, function($val) use ($length, $lang_id) { return $lang_id == substr($val, -$length, $length); });
      foreach ($language_files as $file) {
        if (!preg_match("/$lang_id$/i", $file)) continue;
        $config = $this->localeInstallStorage->read($file);
        $extension_name = str_replace(".$lang_id", '', $file);
        if (isset($config[$lang_id]) && isset($extensions['module'][$extension_name])) {
          $configs[$extension_name][$lang_id] = $this->processConfig($config[$lang_id], $file);
        }
      }
    }

    return $configs;
  }

  /**
   * Undocumented function
   *
   * @param array $config
   * @param [type] $file_name
   * @return void
   */
  protected function processConfig(array $config, $file) {
    $this->moduleHandler->alter('translation', $config);    
    $this->cache->set($file, $config);

    return $config;
  }

  /**
   * Undocumented function
   *
   * @param string $module
   * @param string $langcode
   * @return void
   */
  public function getLocaleTransaltion(string $module, string $langcode) {
    $file_name = $module.'.'.$langcode;
    $extensions = $this->configStorage->read('core.extension');
    if (isset($extensions['module'][$module]) && $this->localeInstallStorage->listAll($file_name)) {
      $config = $this->localeInstallStorage->read($file_name);

      return $this->processConfig($config[$langcode], $file_name);
    }
  }
}