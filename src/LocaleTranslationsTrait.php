<?php
/**
 * @file
 */

namespace Drupal\locale_trans_file;

use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Trait LocaleTranslationsTrait
 *
 * @package Drupal\locale_trans_file
 */
Trait LocaleTranslationsTrait {
  protected $stringLocaleTranslation;
  protected $translationsBase;
  protected $stringTranslation;

  /**
   * Set translation file source base
   * @param $module
   * @param $base_source
   */
  public function setTranslationSource($module, $base_source = '') {
    $this->translationsBase = ($base_source) ? sprintf('%s.%s', $module, $base_source) : $module;
  }

  /**
   * @return string
   */
  public function getTranslationSource() {
    return $this->translationsBase;
  }

  /**
   * @return mixed
   */
  protected function getLocaleStringTranslation() {
    if (!$this->stringLocaleTranslation) {
      $this->stringLocaleTranslation = \Drupal::service('locale_trans_file');
    }

    return $this->stringLocaleTranslation;
  }

  /**
   * Gets the string translation service.
   *
   * @return \Drupal\Core\StringTranslation\TranslationInterface
   *   The string translation service.
   */
  protected function getStringTranslation() {
    if (!$this->stringTranslation) {
      $this->stringTranslation = \Drupal::service('string_translation');
    }

    return $this->stringTranslation;
  }

  /**
   * @param $string
   * @param array $args
   * @param array $options
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function t($string, array $args = [], array $options = []) {
    return new TranslatableMarkup($string, $args, $options, $this->getStringTranslation());
  }

  /** Translate string
   * @param $msg_id
   * @param array $args
   * @param array $options
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function lt($msg_id, array $args = [], array $options = []) {
    if ($this->translationsBase && !preg_match("/^{$this->translationsBase}/", $msg_id)) {
      $string = sprintf('%s.%s', $this->translationsBase, $msg_id);
    }
    else {
      $string = $msg_id;
    }

    return new TranslatableMarkup($string, $args, $options, $this->getLocaleStringTranslation());
  }


  /**
   * Formats a string containing a count of items.
   *
   * @see \Drupal\Core\StringTranslation\TranslationInterface::formatPlural()
   */
  protected function formatPlural($count, $singular, $plural, array $args = [], array $options = []) {
    return new PluralTranslatableMarkup($count, $singular, $plural, $args, $options, $this->getStringTranslation());
  }

  /**
   * Returns the number of plurals supported by a given language.
   *
   * @see \Drupal\locale\PluralFormulaInterface::getNumberOfPlurals()
   */
  protected function getNumberOfPlurals($langcode = NULL) {
    if (\Drupal::hasService('locale.plural.formula')) {
      return \Drupal::service('locale.plural.formula')->getNumberOfPlurals($langcode);
    }
    // We assume 2 plurals if Locale's services are not available.
    return 2;
  }
}