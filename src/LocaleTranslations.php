<?php
/**
 * @file
 */

namespace Drupal\locale_trans_file;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;
use Drupal\locale\LocaleTranslation as DefaultLocaleTranslation;

/**
 * Class LocaleTranslations
 *
 * @package Drupal\locale_translations
 */
class LocaleTranslations implements TranslationInterface {
  protected $localeTranslator;
  protected $defaultLocaleTranslation;
  protected $languageManager;
  protected $defaultLangcode;

  /**
   * LocaleTranslations constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $locale_translations_cache
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(TranslatorInterface $locale_translator,
                              DefaultLocaleTranslation $locale_translation,
                              LanguageManagerInterface $language_manager) {
    $this->localeTranslator = $locale_translator;
    $this->defaultLocaleTranslation = $locale_translation;
    $this->languageManager = $language_manager;
    $this->currentLangcode = $language_manager->getCurrentLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function translate($string, array $args = [], array $options = []) {
    return new TranslatableMarkup($string, $args, $options, $this);
  }

  /**
   * {@inheritdoc}
   */
  public function translateString(TranslatableMarkup $translated_string) {
    $string = $translated_string->getUntranslatedString();
    if (!preg_match('/^(?=.*[a-z])(?=(.*\.){2})(?![^\s]*\s)[a-z0-9_\.]{5,}$/', trim($string))) {
      return $this->defaultLocaleTranslation->translateString($translated_string);
    }

    return $this->doTranslate($string, $translated_string->getOptions());
  }

  /**
   * @param $string
   * @param array $options
   *
   * @return mixed
   */
  protected function doTranslate($string, array $options = []) {
    // If a NULL langcode has been provided, unset it.
    if (!isset($options['langcode']) && array_key_exists('langcode', $options)) {
      unset($options['langcode']);
    }

    // Merge in options defaults.
    $options = $options + [
        'langcode' => $this->currentLangcode,
        'context' => '',
    ];
    $translation = $this->localeTranslator
      ->getStringTranslation($options['langcode'], $string, $options['context']);

    return $translation === FALSE ? $string : $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function formatPlural($count, $singular, $plural, array $args = [], array $options = []) {
    return $count;
  }
}
