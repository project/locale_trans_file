<?php
/**
 * @file
 */

namespace Drupal\locale_trans_file;

use Drupal\Core\Config\ExtensionInstallStorage;
use Drupal\Core\Extension\ExtensionDiscovery;

class InstallStorage extends ExtensionInstallStorage {

  /**
   * {@inheritdoc}
   */
  public function getComponentNames(array $list) {
    $extension = '.' . $this->getFileExtension();
    $pattern = '/' . preg_quote($extension, '/') . '$/';
    $folders = [];
    foreach ($list as $module_name => $extension_object) {
      // We don't have to use ExtensionDiscovery here because our list of
      // extensions was already obtained through an ExtensionDiscovery scan.
      $directory = $this->getComponentFolder($extension_object);
      if (is_dir($directory)) {
        // glob() directly calls into libc glob(), which is not aware of PHP
        // stream wrappers. Same for \GlobIterator (which additionally requires
        // an absolute realpath() on Windows).
        // @see https://github.com/mikey179/vfsStream/issues/2
        $files = scandir($directory);

        foreach ($files as $file) {
          if ($file[0] !== '.' && preg_match($pattern, $file)) {
            $folders[$module_name . '.' . basename($file, $extension)] = $directory;
          }
        }
      }
    }

    return $folders;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilePath($name) {
    $folders = $this->getAllFolders();

    if (isset($folders[$name])) {
      $file_name = preg_replace('/([a-z0-9_-]+)\.([a-z-]+)$/i', '$2', $name);

      return $folders[$name] . '/' . $file_name . '.' . $this->getFileExtension();
    }
  }
}