<?php
/**
 * @file
 */

namespace Drupal\locale_trans_file;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DestructableInterface;
use Drupal\Core\StringTranslation\Translator\TranslatorInterface;
use Drupal\locale\LocaleTranslation as DefaultLocaleTranslation;

/**
 * Class LocaleTranslationsTranslator
 *
 * @package Drupal\locale_trans_file
 */
class LocaleTranslationsTranslator implements TranslatorInterface, DestructableInterface {
  protected $stringTranslations = [];
  protected $cache;
  protected $defaultLocaleTranslation;
  /**
   * Default lang code
   *
   * @var string
   */
  protected $defaultLangCode;

  /**
   * Current lang code
   *
   * @var string
   */
  protected $currentLangCode;

  /**
   * @var LocaleTranslationsManager
   */
  protected $translationManager;

  /**
   * LocaleTranslationsTranslator constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(CacheBackendInterface $cache,
                              DefaultLocaleTranslation $default_string_translation) {
    $this->cache = $cache;
    $this->defaultLocaleTranslation = $default_string_translation;
    $this->defaultLangCode = \Drupal::languageManager()->getDefaultLanguage()->getId();
    $this->currentLangCode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $this->translationManager = \Drupal::getContainer()->get('locale_trans_file.manager');
  }

  /**
   * @param $msgId
   * @param string $lang_id
   *
   * @return bool|mixed
   * @throws \Exception
   */
  public function trans($lang_id = '', $msgId, $context) {
    $lang_id = $lang_id ?: $this->defaultLangCode;
    if (!empty($this->stringTranslations[$msgId])) {
      return $this->stringTranslations[$msgId];
    }

    $msg_path = explode('.', $msgId);
    $module_name = array_shift($msg_path);
    $cid = sprintf('%s.%s', $module_name, $lang_id);
    if (!$cached = $this->cache->get($cid)) {
      if (!$translation = $this->translationManager->getLocaleTransaltion($module_name, $lang_id)) {
        $lang_id = $this->defaultLangCode;
        $cid = sprintf('%s.%s', $module_name, $lang_id);
        $cached = $this->cache->get($cid);
      }
    }
    if ($cached) {
      $translation = $cached->data;
    }
    elseif($translation) {
      $translation = $this->translationManager->getLocaleTransaltion($module_name, $lang_id);
    }

    if ($translation) {
      while($msg_path && $key = array_shift($msg_path)) {
        if (isset($translation[$key])) {
          $translation = $translation[$key];
        }
        else {
          $translation = FALSE;
          break;
        }
      }

      if (is_array($translation) && !empty($translation['msgid'])) {
        $translation = $this->defaultTranslateString($translation, $lang_id, $context);
      }

      if ($translation && !is_array($translation)) {
        return $this->stringTranslations[$msgId] = $translation;
      }
      else {
        $message = sprintf(
          'Locale Translations no Translation found for "%s" language %s',
          $msgId, $lang_id
        );

        throw new \Exception($message);
      }
    }

    return FALSE;
  }

  /**
   * @param $translation
   */
  public function defaultTranslateString(array $translation, $lang_id, $context) {
    $string = FALSE;
    if (!$string = $this->defaultLocaleTranslation->getStringTranslation($this->currentLangCode ,$translation['msgid'], $context)) {
      $string = $this->defaultLocaleTranslation->getStringTranslation($lang_id ,$translation['msgid'], $context);
    }

    if(!$string && isset($translation['msgstr'])) {
      $string = $translation['msgstr'];
    }

    return $string;
  }

  /**
   * @param string $langcode
   * @param string $string
   * @param string $context
   *
   * @return false|string|void
   */
  public function getStringTranslation($langcode, $string, $context) {
    if (preg_match('/^(?=.*[a-z])(?=(.*\.){2})(?![^\s]*\s)[a-z0-9_\.]{5,}$/', trim($string))) {
      return $this->trans($langcode, $string, $context);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $this->stringTranslations = [];
  }

  /**
   * {@inheritdoc}
   */
  public function destruct() {
    $this->stringTranslations = [];
  }
}
